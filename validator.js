class Validator {

    static instance = Validator;

    constructor(data, rules) {
        this.data = data;
        this.rules = rules;

        this.rulePool = {
            'int': new Integer,
            'number': new Numeric
        };

        this.definitions = [];
    }

    static run(data, rules) {
        this.instance = new Validator(data, rules);
        return this.instance.init();
    }

    init() {
        this.digest();
    }

    digest() {
        this.evaluateRules();
    }

    evaluateRules() {
        const that = this;
        Object.keys(this.rules).forEach(function(rule) {
            that.definitions.push(new Definition(rule, that.rules[rule], that));
    
        });
        this.validate();
    }


    validate() {
        console.log(this.definitions);
        

    }
    
}

class Definition {
    constructor(key, rules, instance){
        this.key = key;
        this.rules = rules;
        this.instance = instance;
        this.digest(instance);
    }

    digest(instance) {
        const that = this;
        that.definedRules = [];
        let givenRules = this.rules.split('|');
        givenRules.forEach(function(rule){
            let ruleComponents = rule.split(':'); //if rule contains additional arguments 
            if(!Object.keys(instance.rulePool).includes(ruleComponents[0])){
                console.error('Rule ['+rule+'] does not exist');
            }
            that.definedRules.push({rule: ruleComponents[0], arguments: ruleComponents[1]})
        });
    }



}

class Rule{

}

class Integer extends Rule{
    check(val){
        if(Number.isInteger(val)){
            return true;
        }
        return false;
    }
}

class Numeric extends Rule{
    check(val){
        if(!isNaN(+val)){
            return true;
        }
        return false;
    }
}
